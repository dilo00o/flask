from flask import Flask, render_template, redirect, request
import serial
ser = serial.Serial('COM3',9600)
app = Flask(__name__)
data = {"Lower On": 1, "All Off": 2, "All On": 3, "Custom": 4}


@app.route('/')
def hello_world():
    return render_template("buttons.html")


@app.route('/led', methods=['POST'])
def push():
    print(data[request.form["but"]])
    ser.write(str(data[request.form["but"]]).encode())
    return redirect('/')


if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
